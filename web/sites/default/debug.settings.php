<?php
/**
 * @file
 * amazee.io Drupal 8 development environment configuration file.
 *
 * This file will only be included on development environments.
 *
 * It contains some defaults that the amazee.io team suggests, please edit them as required.
 */

// Expiration of cached pages to 0.
$config['system.performance']['cache']['page']['max_age'] = 0;

// Aggregate CSS files off.
$config['system.performance']['css']['preprocess'] = 0;

// Aggregate JavaScript files off.
$config['system.performance']['js']['preprocess'] = 0;

// Disabling advagg
$config['advagg.settings']['enabled'] = FALSE;

// Show all error messages, with backtrace information.
$config['system.logging']['error_level'] = 'verbose';

// Log mails and send them to mailhog
$config['symfony_mailer.mailer_policy._']['configuration']['symfony_mailer_log'] = [];
$config['symfony_mailer.mailer_policy._']['configuration']['email_transport']['value'] = 'native';

if (getenv('LAGOON_ENVIRONMENT_TYPE') !== 'production') {
  /**
   * Skip file system permissions hardening.
   *
   * The system module will periodically check the permissions of your site's
   * site directory to ensure that it is not writable by the website user. For
   * sites that are managed with a version control system, this can cause problems
   * when files in that directory such as settings.php are updated, because the
   * user pulling in the changes won't have permissions to modify files in the
   * directory.
   */
  $settings['skip_permissions_hardening'] = TRUE;
}
