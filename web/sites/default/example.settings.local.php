<?php
/**
 * @file
 * Example debugging settings and services
 */

// Get debugging settings
if (file_exists(__DIR__ . '/debug.settings.php')) {
  include __DIR__ . '/debug.settings.php';
}

// Get debugging services
if (file_exists(__DIR__ . '/debug.services.yml')) {
  $settings['container_yamls'][] = __DIR__ . '/debug.services.yml';
}

$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['page'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

// include __DIR__ . '/staging.settings.php';
