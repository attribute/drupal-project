<?php

/**
 * @file
 * Lagoon Drupal configuration file.
 *
 * You should not edit this file, please use environment-specific files!
 * They are loaded in this order:
 * - all.settings.php
 *   For settings that should be applied to all environments (dev, prod, staging, docker, etc).
 * - all.services.yml
 *   For services that should be applied to all environments (dev, prod, staging, docker, etc).
 * - production.settings.php
 *   For settings only for the production environment.
 * - production.services.yml
 *   For services only for the production environment.
 * - development.settings.php
 *   For settings only for the development environment (development sites, Docker).
 * - development.services.yml
 *   For services only for the development environment (development sites, Docker).
 * - settings.local.php
 *   For settings only for the local environment, this file will not be committed in Git!
 * - services.local.yml
 *   For services only for the local environment, this file will not be committed in Git!
 *
 */

### Lagoon database connection.
if(getenv('LAGOON')){
  $databases['default']['default'] = [
    'driver' => 'mysql',
    'database' => getenv('MARIADB_DATABASE') ?: 'drupal',
    'username' => getenv('MARIADB_USERNAME') ?: 'drupal',
    'password' => getenv('MARIADB_PASSWORD') ?: 'drupal',
    'host' => getenv('MARIADB_HOST') ?: 'mariadb',
    'port' => 3306,
    'prefix' => '',
  ];
}

### Lagoon reverse proxy settings.
if (getenv('LAGOON')) {
  $settings['reverse_proxy'] = TRUE;
}

### Trusted Host Patterns, see https://www.drupal.org/node/2410395 for more information.
### If your site runs on multiple domains, you need to add these domains here.
if (getenv('LAGOON_ROUTES')) {
  $settings['trusted_host_patterns'] = [
    '^' . str_replace(['.', 'https://', 'http://', ','], ['\.', '', '', '|'], getenv('LAGOON_ROUTES')) . '$', // escape dots, remove schema, use commas as regex separator
  ];
}

### Hash salt.
if (getenv('LAGOON')) {
  $settings['hash_salt'] = hash('sha256', getenv('LAGOON_PROJECT'));
}

// Sendgrid API key
if (getenv('SENDGRID_API_KEY')) {
  $config['symfony_mailer.mailer_transport.sendgrid']['configuration']['pass'] = getenv('SENDGRID_API_KEY');
}

// Configuration location
$settings['config_sync_directory'] = '../config/sync';

// Ignore folders from being scanned during theme cache rebuild
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

// Setting private files path
$settings['file_private_path'] = 'sites/default/files/private';

// Services for all environments.
if (file_exists(__DIR__ . '/services.yml')) {
  $settings['container_yamls'][] = __DIR__ . '/services.yml';
}

if(getenv('LAGOON_ENVIRONMENT_TYPE')){
  // Environment specific settings files.
  if (file_exists(__DIR__ . '/' . getenv('LAGOON_ENVIRONMENT_TYPE') . '.settings.php')) {
    include __DIR__ . '/' . getenv('LAGOON_ENVIRONMENT_TYPE') . '.settings.php';
  }

  // Environment specific services files.
  if (file_exists(__DIR__ . '/' . getenv('LAGOON_ENVIRONMENT_TYPE') . '.services.yml')) {
    $settings['container_yamls'][] = __DIR__ . '/' . getenv('LAGOON_ENVIRONMENT_TYPE') . '.services.yml';
  }
}

// Get project specific settings
if (file_exists(__DIR__ . '/custom/settings.php')) {
  include __DIR__ . '/custom/settings.php';
}
// Get project specific services
if (file_exists(__DIR__ . '/custom/services.yml')) {
  $settings['container_yamls'][] = __DIR__ . '/custom/services.yml';
}

// Last: this servers specific settings files.
if (file_exists(__DIR__ . '/settings.local.php')) {
  include __DIR__ . '/settings.local.php';
}
// Last: This server specific services file.
if (file_exists(__DIR__ . '/services.local.yml')) {
  $settings['container_yamls'][] = __DIR__ . '/services.local.yml';
}
