#!/usr/bin/env bash

set -ve

if [ "NODE_VERSION" = "12" ] ; then
  eval 'apk add nodejs=12.20.1-r0'
fi
#if [ "NODE_VERSION" = "14" ] ; then
#  eval "apk del --no-cache nodejs-current --repository http://dl-cdn.alpinelinux.org/alpine/edge/main \&\& apk add --no-cache nodejs --repository http://dl-cdn.alpinelinux.org/alpine/v3.11/main/"
#fi
