#!/usr/bin/env bash

set -ve

npm ci --prefer-offline --no-audit
npm run gulp
