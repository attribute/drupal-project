ARG CLI_IMAGE
FROM ${CLI_IMAGE} as cli

FROM uselagoon/nginx-drupal

COPY --from=cli /app /app

ENV DOMAIN=domain.ch
ENV ESCAPED_DOMAIN='escapeddomain\.ch'
ENV WWW_REDIRECT=non-www

# Add redirects
COPY lagoon/redirects-map.conf /etc/nginx/redirects-map.conf

RUN if [ ${WWW_REDIRECT} == "www" ]; \
      then echo "~^${ESCAPED_DOMAIN} https://www.domain.ch\$request_uri;" >> /etc/nginx/redirects-map.conf ; \
      else echo "~^www\.${ESCAPED_DOMAIN} https://domain.ch\$request_uri;" >> /etc/nginx/redirects-map.conf ; \
    fi

# Define where the Drupal Root is located
ENV WEBROOT=web
