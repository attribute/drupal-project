FROM uselagoon/php-8.2-cli-drupal
COPY scripts /app/scripts

ENV NODE_VERSION=

RUN /app/scripts/docker/install-node.sh
# RUN /app/scripts/docker/install-packages.sh

ENV THEME_NAMES=contrib/starterkit
ENV THEME_DEPLOYMENTS=npm

COPY composer.json composer.lock /app/
COPY scripts /app/scripts
COPY patches /app/patches
RUN time composer install --no-dev --apcu-autoloader --optimize-autoloader --prefer-dist

COPY . /app

RUN /app/scripts/frontend/build.sh

# Define where the Drupal Root is located
ENV WEBROOT=web
